var express = require('express');
var router = express.Router();
var crypto = require('crypto');

var User = require('../../models/user');

router.get('/',function(req,res,next){
	res.render('main/index');
});

router.get('/login',function(req,res,next){
	res.render('main/login');
});

router.post('/login',function(req,res,next){
	var id = req.body.login_userID;
	var pw = req.body.login_password;
	//암호화
	var key = '★와카★';
	var cipher = crypto.createCipher('aes192',key);
	cipher.update(pw,'utf8','base64');
	var cryptoPassword = cipher.final('base64');
	console.log(cryptoPassword);
	//아이디가 있는지 비밀번호맞는지
	User.findOne({ user_id : id , user_password : cryptoPassword },function(err,doc){
		if(doc == '' || doc == null){
			res.render('main/login', { title_name :'로그인 오류    비밀번호가 맞지 않습니다.'});
		}else{
			req.session.user = doc ;
			res.redirect('/');
		}
	});
	
});

router.get('/loginCheck/:id',function(req,res,next){
	var id = req.params.id;
	//아이디가 존재하는지
	if(id != null && id != ''){
		User.findOne({ user_id : id },function(err,docs){
			if(err){
				console.error(err);
			}else{
				if(docs == ''|| docs == null){
					res.end('N');
				}else{
					if(docs.user_id == id){
						res.end('Y');
					}else{
						res.end('N');
					}
				}
			}
		});
	}
});

router.get('/search',function(req,res,next){
	res.render('main/search');
});

router.post('/join',function(req,res,next){
	var user = new User();
	//암호화
	var pw = req.body.join_password;
	var key = '★와카★';
	var cipher = crypto.createCipher('aes192',key);
	cipher.update(pw,'utf8','base64');
	var cryptoPassword = cipher.final('base64');
	//추가
	user.addUser({
		user_id : req.body.join_userID
		, user_name : req.body.join_name
		, user_password : cryptoPassword
	});
	res.status(201).json({id : req.body.join_userID , pw : req.body.join_password});
});

router.get('/checkID/:id',function(req,res){
	User.findOne({ user_id : req.params.id },function(err, docs){
		if(err){
			console.log(err);
		}else{
			if(docs == ''|| docs == null){
				//없을경우
				res.end('N');
            }else{
            	//있는데 아이디랑 다를경우 있나?
                if(docs.user_id == req.params.id){
                    res.end('Y');
                }else{
                    res.end('N');
                }
            }
		}
	});
});

router.get('/logout',function(req,res,next){
	//세션제거
	req.session.destroy(function(err){
		if(err){
			console.error(err);
			res.redirect('/');
		}else{
			res.redirect('/');
		}
	});
});

module.exports = router;